import { Component } from '@angular/core';

import { Platform, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { secretToken, objectMsg } from './utils/values';
import { UtilsService } from './services/utils.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  private user: any;
  private links: Array<any> = [];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private navController: NavController,
    private utilsService: UtilsService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      if (!this.user) {
        this.logout();
      }
      console.log("iniciando!");
      // this.utilsService.setColorTheme("red", "#CE93D8", "#FFFF00");
    });
  }

  public setUser(obj: any) {
    console.log("SetUser");
    let user = obj[objectMsg.OBJ];
    console.log(user);
    if (user[secretToken.TOKEN] && user[secretToken.TOKEN] != "undefined") {
      console.log("Tem token");
      
      sessionStorage.setItem(secretToken.TOKEN, user[secretToken.TOKEN]);
      this.navController.navigateRoot("/home");
    }
    if (user) {
      this.user = user;
      this.links = this.createLinks(user)
    }
  }

  public createLinks(usuario: any): Array<any> {
    let links = [];
    let perfis = usuario["perfis"];
    if (perfis) {
      perfis.forEach(perfil => {
        let acoes = perfil["acoes"];
        if (acoes) {
          acoes.forEach(acao => {
            if (acao["menu"]) {
              let menu = links.find((l) => l["menu"] == acao["menu"]);
              if (menu) {
                menu["links"].push({ "nome": acao["nome"], "rota": acao["rota"], "icon": acao["icon"] })
              } else {
                links.push({ "menu": acao["menu"], "links": [{ "nome": acao["nome"], "rota": acao["rota"], "icon": acao["icon"] }] });
              }
            } else if (acao["rota"]) {
              links.push({ "nome": acao["nome"], "rota": acao["rota"], "icon": acao["icon"] });
            }
          });
        }
      });
    }
    return links;
  }

  public logout() {
    this.user = null;
    this.utilsService.removeValueStorege(secretToken.TOKEN);
    this.navController.navigateRoot("/login");
  }
}
