import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactTypePage } from './contact-type.page';

describe('ContactTypePage', () => {
  let component: ContactTypePage;
  let fixture: ComponentFixture<ContactTypePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactTypePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactTypePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
