import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactTypeCadPage } from './contact-type-cad.page';

describe('ContactTypeCadPage', () => {
  let component: ContactTypeCadPage;
  let fixture: ComponentFixture<ContactTypeCadPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactTypeCadPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactTypeCadPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
