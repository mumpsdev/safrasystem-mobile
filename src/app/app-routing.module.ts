import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './pages/home/home.module#HomePageModule'
  },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },  { path: 'contact-type', loadChildren: './pages/contact-type/contact-type.module#ContactTypePageModule' },
  { path: 'contact-type-cad', loadChildren: './pages/contact-type/contact-type-cad/contact-type-cad.module#ContactTypeCadPageModule' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
