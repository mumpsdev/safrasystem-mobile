import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ContactTypePage } from './contact-type.page';
import { TranslateModule } from '../../utils/translate/translate.module';

const routes: Routes = [
  {
    path: '',
    component: ContactTypePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ContactTypePage],
  entryComponents:[
  ]
})
export class ContactTypePageModule {}
