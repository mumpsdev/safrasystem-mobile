import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';
import { URLs } from '../utils/values';

@Injectable({
  providedIn: 'root'
})
export class ContactTypeService {

  constructor(private httpClient: HttpClient) { }

  public list(filterUrl: string){
    return this.httpClient.get(`${environment.API + URLs.CONTACT_TYPE}?fields=id,name&${filterUrl}`);
  }

  public create(contactType: any){
    return this.httpClient.post(environment.API + URLs.CONTACT_TYPE, contactType)
  }

  public update(contactType: any){
    return this.httpClient.put(environment.API + URLs.CONTACT_TYPE + "/" + contactType.id , contactType)
  }
  
  public delete(id: string){
    return this.httpClient.delete(environment.API + URLs.CONTACT_TYPE + "/" + id)
  }


}
