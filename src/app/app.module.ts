import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TranslateModule } from './utils/translate/translate.module';
import { AnimationService, AnimatesDirective } from 'css-animator';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { AuthInterceptor } from './services/auth.interceptor';
import { ContactTypeCadPage } from './pages/contact-type/contact-type-cad/contact-type-cad.page';
import { FormsModule } from '@angular/forms';
// import { ContactTypeCadPage } from './pages/contact-type/contact-type-cad/contact-type-cad.page';



@NgModule({
  declarations: [
    AppComponent,
    AnimatesDirective,
    ContactTypeCadPage
  ],
  entryComponents: [
    ContactTypeCadPage
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    TranslateModule,
    FormsModule,
    IonicModule.forRoot(),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    StatusBar,
    SplashScreen,
    AnimationService,
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}
  ],
  exports:[
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
