import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
// import {Observable}  from 'rxjs/Observable';
import { Observable } from "rxjs";
import { finalize, tap } from "rxjs/operators";
import { secretToken } from '../utils/values';
import { UtilsService } from './utils.service';

@Injectable({
    providedIn: 'root'
})
export class AuthInterceptor implements HttpInterceptor {
    constructor(
        private utilService: UtilsService
    ) {}

    public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        console.log("chegou!");
        let ok: string;
        let newReq = this.setTokenSessionStory(req);
        return next.handle(newReq).pipe(
            tap(
                event => { 
                    ok = event instanceof HttpResponse ? 'succeeded' : ''

                    console.log(event);
                }, error => {
                    ok = 'failed';
                    console.log(error);
                }
            ),finalize(() => {})
          );
    }

    private setTokenSessionStory(req: HttpRequest<any>){
        let token = this.utilService.getValueStorege(secretToken.TOKEN);        
        console.log("token");
        console.log(token);
        if(token && token != "undefined"){
            let newReq = req.clone({
                headers: req.headers.set(secretToken.TOKEN, token)
            });
            return newReq;
        }else{
            return req;
        }
    }
}