import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ContactTypeCadPage } from './contact-type-cad.page';
import { TranslateModule } from '../../../utils/translate/translate.module';

const routes: Routes = [
  {
    path: '',
    component: ContactTypeCadPage
  }
];

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    IonicModule,
    TranslateModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ContactTypeCadPage]
})
export class ContactTypeCadPageModule {}
