import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ContactTypeService } from '../../../services/contact.type.service';
import { take } from 'rxjs/operators';
import { UtilsService } from '../../../services/utils.service';
import { objectMsg } from '../../../utils/values';
import { async } from 'q';

@Component({
  selector: 'app-contact-type-cad',
  templateUrl: './contact-type-cad.page.html',
  styleUrls: ['./contact-type-cad.page.scss'],
})
export class ContactTypeCadPage implements OnInit {
  private contactType: any = {};
  @Input() selectedContactType: any;
  
  constructor(
    private modelController: ModalController,
    private contactTypeService: ContactTypeService,
    private utilService: UtilsService
    ) { }

  ngOnInit() {
    if(this.selectedContactType){
      this.contactType = this.selectedContactType;
    }
  }

  async save(){

  }

  async close(){
    await this.modelController.dismiss();
  }

  async update(){
    await this.utilService.loading(null);
    this.contactTypeService.update(this.contactType).pipe(take(1)).subscribe(async data => {
      await this.utilService.showMesseges(data);
      await this.close();
    }, async error => await this.utilService.showMesseges(error));
  }

  async create(){
    await this.utilService.loading(null);
    this.contactTypeService.create(this.contactType).pipe(take(1)).subscribe(async data => {
      await this.utilService.showMesseges(data).then( () => this.close());
    }, async error => await this.utilService.showMesseges(error));
  }

}