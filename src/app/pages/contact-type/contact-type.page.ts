import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { take } from 'rxjs/operators';
import { UtilsService } from '../../services/utils.service';
import { objectMsg } from '../../utils/values';
import { ContactTypeService } from '../../services/contact.type.service';
import { async } from '@angular/core/testing';
import { ContactTypeCadPage } from './contact-type-cad/contact-type-cad.page';
import { TranslateService } from '../../utils/translate/translate.service';
import { LANG_PT_TRANS } from '../../utils/translate/lang-pt';

@Component({
  selector: 'app-contact-type',
  templateUrl: './contact-type.page.html',
  styleUrls: ['./contact-type.page.scss'],
})
export class ContactTypePage implements OnInit {
  private contactTypes = [];
  private selectedContactType: any;
  private listFilters = [];

  constructor(
    private contactTypeService: ContactTypeService,
    private utilService: UtilsService,
    private translate: TranslateService,
    private detect: ChangeDetectorRef
  ) { }
  
  ngOnInit() {
    this.listContactTypes();
  }
  
  async listContactTypes(){
    let filters = this.utilService.createFiltersList(this.listFilters);
    console.log(filters);
    
    await this.utilService.loading(null);
    this.contactTypeService.list(filters).pipe(take(1)).subscribe(async data => {
      this.contactTypes = data[objectMsg.LIST];
      this.detect.detectChanges();
      console.log(this.contactTypes);
      await this.utilService.showMesseges(data);
    }, error => this.utilService.showMesseges(error));
  }

  async showCad(selectedContactType: any){
    let modal = await this.utilService.showModal(ContactTypeCadPage, {"selectedContactType": selectedContactType});
    modal.onDidDismiss().then( () => {
      this.selectedContactType = null;
      this.listContactTypes();
    })
  }

  async showEdit(){
    this.showCad(this.selectedContactType);
  }

  async onClick(contactType: any){
    if(contactType == this.selectedContactType){
      this.selectedContactType = null;
    }else{
      this.selectedContactType = contactType;
    }
  }

  async delete(id): Promise<any>{
    await this.utilService.loading(null);
    this.contactTypeService.delete(id).pipe(take(1)).subscribe(async data => {
      await this.utilService.showMesseges(data);
      await this.listContactTypes();
    }, error => this.utilService.showMesseges(error))
  }
  
  async showDelete(){
    let msgConfirm = this.translate.getValue(LANG_PT_TRANS.DESEJA_REMOVER, this.selectedContactType.name);
    let alertDelete = await this.utilService.confirm(null, msgConfirm, async () => {
      await this.delete(this.selectedContactType.id);
    }, () =>{}, null, null);
    
  }

  async showSearch(){
    let title = this.translate.getValue(LANG_PT_TRANS.SEARCH);
    let filter = this.translate.getValue(LANG_PT_TRANS.FILTER);
    let clean = this.translate.getValue(LANG_PT_TRANS.CLEAN);
    let placeholder = this.translate.getValue(LANG_PT_TRANS.NAME);
    let valueName = this.listFilters[0] ? this.listFilters[0]["name"] : null;
    let alert = await this.utilService.showAlert(title, null, null,
      [//Botões
        {"text": clean, "handler": async () => {this.listFilters = []; this.listContactTypes()}},
        {"text": filter, "handler": (data) => {
            this.listFilters = [];
            console.log("Pesquisar");
            console.log(data);
            if(data[0]){this.listFilters.push({"name": data[0]});
            }else{this.listFilters = []}
            this.listContactTypes();
         }}
      ], [//Input de pesquisa name
        {"type": "text", "placeholder": placeholder, "value": valueName},
      ]);
  }

}
