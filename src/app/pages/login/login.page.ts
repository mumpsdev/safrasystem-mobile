import { Component, OnInit } from '@angular/core';
import { take } from 'rxjs/operators';
import { AppComponent } from '../../app.component';
import { LoginService } from '../../services/login.service';
import { UtilsService } from '../../services/utils.service';
import { objectMsg, secretToken } from '../../utils/values';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  userLogged: any = {};

  constructor(
    private utilService: UtilsService,
    private loginService: LoginService,
    private app: AppComponent
  ) {}

  ngOnInit() {
  }
  
  async proximo(){
    await this.utilService.loading(null);
    this.loginService.getUserNameUserLogged(this.userLogged.login).pipe(
      take(1)
      ).subscribe(data => {
      console.log(data);
      this.userLogged = data[objectMsg.OBJ];
      console.log(this.userLogged);
      this.utilService.showMesseges(data);
    }, error => {
      this.utilService.showMesseges(error);
    })
  }
  
  async login(){
    this.utilService.loading(null);
    this.loginService.login(this.userLogged).pipe(
        take(1)
      ).subscribe(data => {
        this.app.setUser(data);
        this.utilService.showMesseges(data);
      }, error => {
        this.utilService.showMesseges(error);

    });
  }

  async voltar(){
    this.userLogged = {};
  }

  async isTokenValid(data: any){

    if(data[objectMsg.OBJ] && data[objectMsg.OBJ][secretToken.TOKEN] && data[objectMsg.OBJ][secretToken.TOKEN] != "undefinede"){
      return true;
    }
    return false;
  }

}
