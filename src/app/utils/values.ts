export const URLs = {//Urls rest.
     //---------------- Auth
    "AUTH_LOGIN": "/api/v1/login-auth/:login",
    "AUTH_AUTHENTICATE": "/api/v1/authenticate",
    "AUTH_ALL": "/*", 
    //---------------- Contact Type
    "CONTACT_TYPE": "/api/v1/contact-type",
    "CONTACT_TYPE_ID": "/api/v1/contact-type/:id"
}

export const typeMsg = {//Tipos de mensagens
    "DANGER": "msgErro",
    "SUCCESS": "msgSuccesso",
    "INFO": "msgInfo",
    "ALERT": "msgAlert"
}

export const objectMsg = {//Objetos de retorno
    "STATUS": "status",
    "ERROR": "error",
    "OBJ": "obj",
    "LIST": "list",
    "LIST_MSG": "listMsg",
    "PAGE": "page",
    "TOTAL_PAGES": "totalPages",
    "LIMIT": "limit",
    "RANGE_INIT": "rangeInit",
    "RANGE_END": "rangeEnd",
    "ASC": "asc",
    "DESC": "desc",
    "TOTAL_ROWS": "totalRows",
}

export const secretToken ={
    "TOKEN": "x-access-token",
    "SECRET_PUBLIC" : "projeto-publico"
}