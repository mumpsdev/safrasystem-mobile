export const LANG_EN_NAME = 'en';

export const LANG_EN_TRANS = {
        "BEM_VINDOS": "Welcome",
        "PESQUISAR": "Search",
      //------------------------------------------------------------------------    Erros de stastus do servidor
        "statusDefault": "Error trying to connect to server.",
        "status302": "The resource has been temporarily moved to another URI.",
        "status304": "O recurso não foi alterado.",
        "status400": "The feature has not been changed.",
        "status401": "A URI especificada exige autenticação do cliente. O cliente pode tentar fazer novas requisições.",
        "status403": "The specified URI requires client authentication. The client can try to make new requests.",
        "status404": "The server did not find any matching URIs.",
        "status405": "O método especificado na requisição não é válido na URI. A resposta deve incluir um cabeçalho Allow com uma lista dos métodos aceitos.",
        "status410": "The method specified in the request is not valid in the URI. The response must include a Allow header with a list of accepted methods.",
        "status500": "The server was unable to complete the request because of an unexpected error.",
        "status502": "The server, acting as a proxy or gateway, received an invalid response from the upstream server to which it made a request.",
        "status503": "The server, acting as a proxy or gateway, received an invalid response from the upstream server to which it made a request.",
};